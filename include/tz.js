if(window.addEventListener) {
    window.addEventListener('load', function () {

        var canvas, context, tool;

        function init () {
            // Находим canvas элемент
            canvas = document.getElementById('tablet');

            if (!canvas) {
                alert('Ошибка! Canvas элемент не найден!');
                return;
            }

            if (!canvas.getContext) {
                alert('Ошибка: canvas.getContext не существует!');
                return;
            }

            // Получаем 2D canvas context.
            context = canvas.getContext('2d');
            if (!context) {
                alert('Ошибка: getContext! не существует');
                return;
            }

            tool = new tool_pencil();
            canvas.addEventListener('mousedown', ev_canvas, false);
            canvas.addEventListener('mousemove', ev_canvas, false);
            canvas.addEventListener('mouseup',   ev_canvas, false);
        }

        // Здесь мы будем ловить движения мыши
        function tool_pencil () {
            var tool = this;
            this.started = false;


            this.mousedown = function (ev) {
                context.beginPath();
                context.moveTo(ev._x, ev._y);
                tool.started = true;
            };

            // Эта функция вызывается каждый раз, когда вы перемещаете мышь.
            // Но рисование происходит только когда вы удерживаете кнопку мыши
            // нажатой.
            this.mousemove = function (ev) {
                if (tool.started) {
                    context.lineTo(ev._x, ev._y);
                    context.stroke();
                }
            };

            // Событие при отпускании мыши
            this.mouseup = function (ev) {
                if (tool.started) {
                    tool.mousemove(ev);
                    tool.started = false;
                }
            };
        }

        // Эта функция определяет позицию курсора относительно холста
        function ev_canvas (ev) {
            if (ev.layerX || ev.layerX == 0) { // Firefox
                ev._x = ev.layerX;
                ev._y = ev.layerY;
            } else if (ev.offsetX || ev.offsetX == 0) { // Opera
                ev._x = ev.offsetX;
                ev._y = ev.offsetY;
            }

            // Вызываем обработчик события tool
            var func = tool[ev.type];
            if (func) {
                func(ev);
            }
        }

        init();

    }, false); }

function saveCanvasAsImageFile(){
    alert('hi88');
    //var image = getImage(document.getElementById("tablet"));
    var photo = document.getElementById("tablet").toDataURL('image/png').replace(/data:image\/png;base64,/, '');

    $.ajax({
        method: 'POST',
        url: '/include/photo_upload.php',
        data: {
            photo: photo
        },
        success: function(data){
            alert( "Прибыли данные: " + data );
        }
    });
    //saveImage(image);
}

function saveImage(image) {
    var link = document.createElement("a");

    link.setAttribute("href", image.src);
    link.setAttribute("download", "canvasImage");
    link.click();
}

function getImage(canvas){
    var imageData = canvas.toDataURL();

    var image = new Image();
    image.src = imageData;
    return image;
}